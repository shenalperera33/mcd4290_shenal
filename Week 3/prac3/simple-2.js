task2()

function task2(){
    var outputAreaRef = document.getElementById("outputArea2");
    var output = "";
    var testObj = {
            number: 1,
            string: "abc",
            array: [5, 4, 3, 2, 1],
            boolean: true
        };
    
    var testObj2 = {
        name: "Bob",
        age: 32,
        married: true,
        children: ["Jack", "Jill"]
        };
    
    function objectToHTML(obj){
        var keys =  Object.keys(testObj);
        var objectText = "";
        for(var key of keys){
            objectText += key + " : " + testObj[key] + "<br>";
        }
        return objectText;
    }
    
    output += objectToHTML(testObj);
    
    output += "<br><br>";
    
    output += objectToHTML(testObj2);
    
    outputAreaRef.innerHTML = output;
 }